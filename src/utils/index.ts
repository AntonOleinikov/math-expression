import {v4} from 'uuid';

const toMathExpression = (value: string) => {
  return value
    .replace('plus', '+')
    .replace('minus', '-')
    .replace(/[a-z/?]/gi, '');
};

const toMathPreview = (value: string) => {
  return value
    .replace('What is ', '')
    .replace('?', ' equals');
};



export const calcExpression = (expression: string) => {
  try {
    const value = expression.replace(/\s/g,'');
    const questionPattern = (/(W|w)hatis\d+(plus|minus)\d+\?/g);

    if (questionPattern.test(value)) {
      const mathExpression = toMathExpression(value);

      const uuid: string = v4();
      const result: number = eval(mathExpression);
      const shortExpression = toMathPreview(expression);
      const preview = `${shortExpression} ${result}`;

      return {
        uuid,
        result,
        expression,
        preview
      };
    } else {
      throw new Error()
    }
  } catch (e) {
    throw new Error()
  }
};
