import styled from 'styled-components';

export const H3Title = styled.h3`
  margin-bottom: 20px;
`;

export const ExpressionResultsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const AsideContainer = styled.div`
  background-color: rgb(253,254,254);
  width: 100%;
  overflow-y: auto;
  
  h4 {
    height: calc(100% - 60px);
    display: flex;
    justify-content: center;
    align-items: center;
    color: rgb(117, 117, 117)
  }
`;

export const ContentContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 600px;
  border-radius: 7px;
  margin: 0 auto 20px auto;
`;

export const ResultValue = styled.div`
  margin-top: 35px;
  font-size: 20px;
  font-weight: 500;
`;

