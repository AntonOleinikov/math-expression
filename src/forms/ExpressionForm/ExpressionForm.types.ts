export type TExpressionForm = {
  hint?: string;
  value: string;
  error?: string;
  onSubmit: (value: string) => void;
  onChange: (value: string) => void;
}

