import React, { ChangeEvent, Fragment } from 'react';
import { Button, TextField } from '@components';
import { Container, Hint } from './ExpressionForm.styles';
import { TExpressionForm } from './ExpressionForm.types';

const ExpressionForm = ({value, hint, onSubmit, onChange, error}: TExpressionForm) => {
  return (
    <Fragment>
      {
        hint &&
        <Hint>
          {hint}
        </Hint>
      }
      <Container>
        <TextField
          data-cy="calc-expression-field"
          value={value}
          error={error}
          placeholder="Write the expression..."
          onChange={({target}: ChangeEvent<HTMLInputElement>) => {
            onChange(target.value);
          }}
        />

        <Button
          data-cy="calc-expression-submit"
          style={{
            marginLeft: 10
          }}
          onClick={() => {
            onSubmit(value)
          }}
        >
          Calc
        </Button>
      </Container>
    </Fragment>
  )
};

export default ExpressionForm;
