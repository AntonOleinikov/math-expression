import { ButtonHTMLAttributes, ReactNode } from 'react';

export type TButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: ReactNode
}
