import styled from 'styled-components';

export const Logo = styled.a`
  font-size: 22px;
  font-weight: 500;
`;

export const Container = styled.div`
  background-color: rgb(215,220,237);
`;

export const InnerContainer = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
`;
