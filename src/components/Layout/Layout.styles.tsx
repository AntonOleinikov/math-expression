import styled from 'styled-components';
import { TLayout } from './Layout.types';

export const Container = styled.div<Partial<TLayout>>`
  background-color: rgb(215,220,237);
  height: 100%;
  display: flex;
  
  @media (max-width: 640px) {
    flex-direction: column;
  }
`;

export const HeaderContainer = styled.div`
  width: 100%;
`;

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 75%;
  
  @media (max-width: 640px) {
    width: 100%;
  }
`;

export const AsideContainer = styled.div`
  display: flex;
  width: 25%;
  min-width: 300px;
  
  @media (max-width: 640px) {
    width: 100%;
  }
`;


