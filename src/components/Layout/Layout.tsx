import React from 'react';
import { Header } from '@components';
import { Container, ContentContainer, AsideContainer } from './Layout.styles';
import { TLayout } from './Layout.types';

const Layout = ({ aside, content }: TLayout) => {
  return (
    <Container>
      <ContentContainer>
        <Header />
        { content }
      </ContentContainer>
      <AsideContainer>
        { aside }
      </AsideContainer>
    </Container>
  )
};

export default Layout;
